<?php

namespace KrrAcfFiltering;

/**
 * Class Update
 *
 * @package KrrAcfFiltering
 */
class Update
{
	/**
	 * @var Update|null  Instance de la class Update
	 */
	public static $instance = null;

	/**
	 * Update constructor.
	 */
	public function __construct()
	{
		add_action('save_post', [$this, 'setFilterToPost'], 1, 2);
	}

	/**
	 * Get the instance of the current class
	 *
	 * @return Update|null
	 */
	public static function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new Update();
		}

		return self::$instance;
	}

	/**
	 * Add the selected filter to the field group
	 *
	 * @param $post_id
	 * @param $post
	 */
	public function setFilterToPost($post_id, $post)
	{
		$Plugin     = Plugin::getInstance();
		$Categories = Categories::getInstance();

		/* Only set for acf-field-group ! */
		if ('acf-field-group' !== $post->post_type || !isset($_POST[$Plugin->selectorFieldName])) {
			return;
		}

		$value = $_POST[$Plugin->selectorFieldName];

		if ($value === 'default') {
			$value = $this->getPostLocation($post_id);
		}

		/* Get the selected filter and add its taxonomy term to the post (replace the oldest one) */
		$term = get_term_by('slug', $value, $Categories->getTaxonomyName());
		wp_set_post_terms($post_id, $term->term_id, $Categories->getTaxonomyName(), false);
	}

	/**
	 * Get the location of the field group
	 *
	 * @param int $post_id
	 *
	 * @return int|string|null
	 */
	private function getPostLocation(int $post_id)
	{
		$fieldGroup = acf_get_field_group($post_id);
		$Categories = Categories::getInstance();

		if (!isset($fieldGroup['location']) || !isset($fieldGroup['location'][0]) || !isset($fieldGroup['location'][0][0])) {
			return null;
		}

		$fieldLocation = $fieldGroup['location'][0][0]['param'];

		$return = null;

		foreach ($Categories->getDefaultCategories(false) as $group => $locations) {
			if (in_array($fieldLocation, $locations)) {
				$return = $group;
				break;
			}
		}

		return $return;
	}
}