<?php

namespace KrrAcfFiltering;

/**
 * Class TaxonomyList
 *
 * @package KrrAcfFiltering
 */
class TaxonomyList
{

	public function __construct()
	{
		add_action('admin_head', [$this, 'tagsTableStyle']);
	}

	/**
	 * Add style to prevent editing the default categories
	 */
	public function tagsTableStyle() {
		/* Security to add stylesheet only on the page we want */
		if (!$this->isListPage()) {
			return false;
		}

		/* Get the Categories instance */
		$Categories = Categories::getInstance();

		/* Get the default terms ids */
		$terms = get_option($Categories->getTermsOptionName());

		/* Create the different selectors based on the ids list */
		$titleSelector = '';
		$actionsSelector = '';
		foreach ($terms as $term) {
			$base = '.taxonomy-krr-acf-filtering #tag-' . $term;

			$titleSelector .= $base . ' .row-title,';
			$actionsSelector .= $base . ' .row-actions,';
		}

		/* Remove the last "," */
		$titleSelector = trim($titleSelector, ',');
		$actionsSelector = trim($actionsSelector, ',');


		/* Add the stylesheet */
		echo <<<HTML
<style type="text/css">
	{$titleSelector}{pointer-events: none;color:#333;}
	{$actionsSelector}{display: none;}
</style>
HTML;
	}

	/**
	 * Check if we are in the list page of tags edition
	 *
	 * @return bool
	 */
	public function isListPage()
	{
		$screen = get_current_screen();
		return 'edit-krr-acf-filtering' === $screen->id;
	}
}