<?php

namespace KrrAcfFiltering;

/**
 * Class Categories
 *
 * @package KrrAcfFiltering
 */
class Categories
{
	/**
	 * @var boolean Check if the plugin has already been initialized
	 */
	private $initOptionName = 'krr-acf-filtering-initialized';

	/**
	 * @var boolean Check if the plugin has already been initialized
	 */
	private $termsOptionName = 'krr-acf-filtering-terms';

	/**
	 * @var array Array of the available filters with filed groups
	 */
	private $list = [];

	/**
	 * @var array Array of all available filters
	 */
	private $fullList = [];

	/**
	 * @var string Name of the selector field in edit page
	 */
	private $taxonomyName = 'krr-acf-filtering';

	/**
	 * @var Categories|null  Instance de la class Categories
	 */
	private static $instance = null;

	/**
	 * Plugin constructor.
	 */
	public function init()
	{
		add_action('init', [$this, 'registerTaxonomy'], 0);

		if (get_option($this->getInitOptionName()) === '1') {
			add_action('init', [$this, 'setTermsLists'], 1);
		} else {
			$this->setDefaultList();
			add_action('init', [$this, 'addTaxonomyTerms'], 5);
		}

	}

	/**
	 * Get the instance of the current class
	 *
	 * @return Categories|null
	 */
	public static function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new Categories();
		}

		return self::$instance;
	}

	/**
	 *  Set the default filters list for ACF group fields
	 */
	private function setDefaultList()
	{
		foreach ($this->getDefaultCategories() as $entry) {
			if ($entry === 'post_type') {
				$label = __('Post Types', 'krr-acffg');
			} elseif ($entry === 'page') {
				$label = __('Pages', 'krr-acffg');
			} elseif ($entry === 'options_page') {
				$label = __('Options Pages', 'krr-acffg');
			} elseif ($entry === 'taxonomy') {
				$label = __('Taxonomies', 'krr-acffg');
			} elseif ($entry === 'media') {
				$label = __('Medias', 'krr-acffg');
			} elseif ($entry === 'user') {
				$label = __('Users', 'krr-acffg');
			} elseif ($entry === 'menu') {
				$label = __('Menus', 'krr-acffg');
			} elseif ($entry === 'widgets') {
				$label = __('Widgets', 'krr-acffg');
			} elseif ($entry === 'comments') {
				$label = __('Comments', 'krr-acffg');
			} elseif ($entry === 'blocks') {
				$label = __('Blocks', 'krr-acffg');
			} elseif ($entry === 'bulk') {
				$label = __('Bulk', 'krr-acffg');
			} else {
				$label = $entry;
			}

			$this->list[$entry] = $label;
			$this->fullList[$entry] = $label;
		}
	}

	/**
	 * Set the actual filters lists for ACF group fields from the taxonomy terms
	 */
	public function setTermsLists()
	{
		/* Set the list of all the terms */
		$terms = get_terms($this->getTaxonomyName(), ["hide_empty" => false]);

		foreach ($terms as $term) {
			$this->fullList[$term->slug] = $term->name;
		}

		/* Set the list of the terms that have a groupfield */
		$terms = get_terms($this->getTaxonomyName(), ["hide_empty" => true]);

		foreach ($terms as $term) {
			$this->list[$term->slug] = $term->name;
		}
	}

	/**
	 * Get the list of filters
	 *
	 * @param bool $hideIfEmpty
	 *
	 * @return array
	 */
	public function getList($hideIfEmpty = false)
	{
		return ($hideIfEmpty) ? $this->list : $this->fullList;
	}

	/**
	 * Get the reduced list of filters (only those set by the user)
	 *
	 * @return array
	 */
	public function getReducedList()
	{
		$reducedList = $this->fullList;

		foreach ($this->getDefaultCategories() as $defaultCategory) {
			unset($reducedList[$defaultCategory]);
		}

		$base = [
			'default' => __('By default', 'krr-acffg')
		];

		return $base + $reducedList;
	}

	/**
	 * Get an array of the default categories initialized by ACF
	 *
	 * @param bool $onlyKeys
	 *
	 * @return int[]|mixed|string[]|null
	 */
	public function getDefaultCategories($onlyKeys = true)
	{
		$list = json_decode(
			file_get_contents(plugin_dir_path(KRR_ACFFG_FILE) . 'config/filters.json'),
			true
		);

		return ($onlyKeys) ? array_keys($list) : $list;
	}

	/**
	 * Register the taxonomy
	 */
	public function registerTaxonomy()
	{
		$labels = [
			'name'          => _x('ACF Filtering Categories', 'Taxonomy General Name', 'krr-acffg'),
			'singular_name' => _x('ACF Filtering Category', 'Taxonomy Singular Name', 'krr-acffg'),
			'menu_name'     => __('Category', 'krr-acffg'),
		];
		$args = [
			'labels'             => $labels,
			'hierarchical'       => true,
			'public'             => true,
			'show_ui'            => true,
			'show_admin_column'  => false,
			'show_in_nav_menus'  => false,
			'show_tagcloud'      => false,
			'publicly_queryable' => false,
			'rewrite'			 => false
		];
		register_taxonomy($this->taxonomyName, ['acf-field-group'], $args);
	}

	/**
	 * Add the filters to the taxonomy
	 */
	public function addTaxonomyTerms()
	{
		$terms = [];

		foreach ($this->list as $filterSlug => $filterLabel) {
			$term = wp_insert_term($filterLabel, $this->taxonomyName, [
				'slug' => $filterSlug
			]);

			array_push($terms, $term['term_id']);
		}

		/* Set the options storage */
		update_option($this->getInitOptionName(), true);
		update_option($this->getTermsOptionName(), $terms);
	}

	/**
	 * Get taxonomy name
	 *
	 * @return string
	 */
	public function getTaxonomyName()
	{
		return $this->taxonomyName;
	}

	/**
	 * Get Initialized option name
	 *
	 * @return string
	 */
	public function getInitOptionName()
	{
		return $this->initOptionName;
	}

	/**
	 * Get Terms option name
	 *
	 * @return string
	 */
	public function getTermsOptionName()
	{
		return $this->termsOptionName;
	}
}
