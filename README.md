# ACF Filtering
You are using Advanced Custom Fields but you are lost among all your field groups and you can not find quickly the one you want to update ? This plugin will allow you to sort your field groups by categories and you will have only the field groups of a selected category that will be shown.

## Requirements
* Require WordPress 5.0+ / Tested up to 5.7
* Require PHP 7.0+
* Advanced Custom Fields 5.9.0+

## Installation

### Manual

Download and install the plugin using the built-in WordPress plugin installer.
No settings necessary, it's a plug-and-play plugin !


### Composer

Add the following repository source :

    {
        "type": "vcs",
        "url": "https://gitlab.com/kofinorr/wordpress-plugins/acf-filtering.git"
    }

Include `"kofinorr/acf-filtering": "dev-master"` in your composer file for last master's commits or a tag released.  
No settings necessary, it's a plug-and-play plugin !

## How to use
It is really simple. This plugin uses the taxonomy system combine with the locations of the ACF groupfields system.

On the creation/update of your groupfield, you can choose the `By default` option to use the ACF location, your you can use a custom term that you have created.

To create a custom term, you just have to add it in the `Categories` submenu of ACF. Some terms will already be there but can not be edited or removed, it is the default locations of ACF.

Then, when you are on your list of ACF groupfields, you can filter them by choosing one of the available terms of the table, on the left side. 

## Internationalisation
- English (default)
- Français

## License
"ACF Filtering" is licensed under the GPLv3 or later.
