<?php
/**
 * Plugin Name: ACF Filtering
 * Description: The ideal plugin to find your way easily among your ACF groupfields.
 * Version: 1.0.4
 * Author: Kofinorr
 * Author URI: https://kofinorr.damien-roche.fr/
 * Text Domain: krr-acffg
 * Domain Path: /languages
 * License: GPLv2 or later
 */

use \KrrAcfFiltering\Plugin;
use function KrrAcfFiltering\krr_require;

/* Security */
defined('ABSPATH')
or die ('no');

define('KRR_ACFFG_FILE', __FILE__);

require_once plugin_dir_path(KRR_ACFFG_FILE) . 'includes/utility-functions.php';

/* Cal the files */
krr_require('classes/Dependencies.php');
krr_require('classes/Categories.php');
krr_require('classes/Update.php');
krr_require('classes/TaxonomyList.php');
krr_require('classes/Plugin.php');

Plugin::getInstance();
